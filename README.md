# Sync vault kv secrets

## Config file example

```json
{
    "src": {
        "addr": "https://vault.example.com",
        "token": "",
        "mount": "secret",
        "path": "directory_to_sync"
    },
    "dst": {
        "addr": "http://localhost:8200",
        "token": "",
        "mount": "secret",
        "path": "directory_to_sync"
    }
}
```

## Compatibility

| SOURCE KV | DESTINATION KV | SUPPORT |
|-----------|----------------|---------|
| KVv1 | KVv1 | no |
| KVv1 | KVv2 | yes |
| KVv2 | KVv1 | no |
| KVv2 | KVv2 | yes |

## Run

```bash
./sync-vault-kv [-c|--config file]
```
> Default config `config.json`
