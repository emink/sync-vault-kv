package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/hashicorp/vault-client-go"
	"github.com/hashicorp/vault-client-go/schema"
)

type client interface {
	listDir() error
	getDirSecrets() error
	writeKV2Secrets() error
}

type config struct {
	Src options `json:"src"`
	Dst options `json:"dst"`
}

type options struct {
	Addr  string `json:"addr"`
	Token string `json:"token"`
	Mount string `json:"mount"`
	Path  string `json:"path"`
}

type directory struct {
	Path    string             `json:"path"`
	Secrets map[string]*secret `json:"secrets"`
	Subdirs interface{}        `json:"subdirs"`
}

type secret struct {
	Path   string             `json:"path"`
	Name   string             `json:"name"`
	Fields map[string]*string `json:"fields"`
}

// initVaultClient() wrapper around vault.New() and vault.SetToken()
// and return new vault client from given options
func (opt *options) initVaultClient() (c *vault.Client, err error) {
	if c, err = vault.New(
		vault.WithAddress(opt.Addr),
		vault.WithRequestTimeout(10*time.Second),
	); err != nil {
		err = fmt.Errorf("Failed to create new vault client: %s\n", err)
		return
	}

	if err = c.SetToken(opt.Token); err != nil {
		err = fmt.Errorf("Failed to set vault token: %s\n", err)
	}

	return
}

// dumpSecrets() recursively iterate over kv storage version 1 or 2
// and return obtained data
func (opt *options) dumpSecrets(c *vault.Client, ctx context.Context) (data interface{}, err error) {
	var resp *vault.Response[schema.InternalUiReadMountInformationResponse]
	resp, err = c.System.InternalUiReadMountInformation(ctx, opt.Mount)
	if err != nil {
		err = fmt.Errorf("Failed to request kv version: %s\n", err)
		return
	}

	kvVersion, ok := resp.Data.Options["version"]
	if !ok {
		err = fmt.Errorf("Can't parse kv version: %s\n", err)
		return
	}

	if kvVersion == "1" {
		store := kvV1{Path: opt.Path}
		err = store.listDir(c, ctx, opt.Mount)
		data = store
	} else if kvVersion == "2" {
		store := kvV2{Path: opt.Path}
		err = store.listDir(c, ctx, opt.Mount)
		data = store
	}

	return
}

// pushSecrets() require data obtained from dumpSecrets.
// Works only with version 2 destination kv storage
func (opt *options) pushSecrets(c *vault.Client, ctx context.Context, data interface{}) (err error) {
	var resp *vault.Response[schema.InternalUiReadMountInformationResponse]
	resp, err = c.System.InternalUiReadMountInformation(ctx, opt.Mount)
	if err != nil {
		err = fmt.Errorf("Failed to request kv version: %s\n", err)
		return
	}

	kvVersion, ok := resp.Data.Options["version"]
	if !ok {
		err = fmt.Errorf("Can't parse kv version: %s\n", err)
		return
	}

	if kvVersion == "1" {
		err = fmt.Errorf("Write to version v1 is unsupported")
		return
	} else if kvVersion == "2" {
		store, ok := data.(kvV1)
		if !ok {
			err = fmt.Errorf("data is not kvV1")
			return
		}

		store.writeDirSecretsKV2(c, ctx, opt.Mount)
	}

	return
}

// loadConfig() read config from json config file
func loadConfig(file string) (cfg config) {
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		err = fmt.Errorf("Config file '%s' not found: %s\n", file, err)
		log.Fatal(err)
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&cfg)
	return
}

func outputJSON(store interface{}) string {
	jsonData, err := json.Marshal(store)
	if err != nil {
		log.Fatal(err)
	}

	return string(jsonData)
}

func main() {
	var file string
	args := os.Args

	if len(args) == 1 {
		file = "config.json"
	} else if len(args) == 3 {
		if args[1] == "-c" || args[1] == "--config" {
			file = args[2]
		} else {
			err := fmt.Errorf("Use ./'%s' [-c|--config] <file>\n", args[0])
			log.Fatal(err)
		}
	} else if args[1] == "-h" || args[1] == "--help" {
		fmt.Printf("Use ./'%s' [-c|--config] <file>\n", args[0])
		os.Exit(0)
	}

	cfg := loadConfig(file)

	ctx := context.Background()
	srcCli, err := cfg.Src.initVaultClient()
	if err != nil {
		log.Fatal(err)
	}

	srcStore, err := cfg.Src.dumpSecrets(srcCli, ctx)
	if err != nil {
		log.Fatal(err)
	}

	// fmt.Println(outputJSON(srcStore))

	dstCli, err := cfg.Dst.initVaultClient()
	if err != nil {
		log.Fatal(err)
	}

	if err = cfg.Dst.pushSecrets(dstCli, ctx, srcStore); err != nil {
		log.Fatal(err)
	}
}
