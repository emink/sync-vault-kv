package main

import (
	"context"
	"fmt"
	"strings"

	"github.com/hashicorp/vault-client-go"
	"github.com/hashicorp/vault-client-go/schema"
)

type kvV1 directory

// listDir() recursively iterate over kv storage version 1
// populating kvV1 struct while executing itself in subdirectories
// as many time as needed
// mnt: vault mount point
func (kv *kvV1) listDir(c *vault.Client, ctx context.Context, mnt string) (err error) {
	sURI := []string{mnt, kv.Path}
	uri := strings.Join(sURI, "/")

	var resp *vault.Response[map[string]interface{}]

	if resp, err = c.List(ctx, uri); err != nil {
		err = fmt.Errorf("List directory '%s', request failed: %s\n", uri, err)
		return
	}

	data, ok := resp.Data["keys"].([]interface{})
	if !ok {
		err = fmt.Errorf("Can't parse keys from response. Type is not []interface")
		return
	}

	subdirs := make(map[string]*kvV1)
	secrets := make(map[string]*secret)

	for k := range data {
		if v, ok := data[k].(string); ok {
			path := strings.Join([]string{kv.Path, v[:len(v)-1]}, "/")
			if v[len(v)-1:] == "/" {
				subdirs[v] = &kvV1{Path: path}
				if err = subdirs[v].listDir(c, ctx, mnt); err != nil {
					err = fmt.Errorf("Failed to list '%s': %s\n", path, err)
					return
				}
			} else {
				secrets[v] = &secret{Path: kv.Path, Name: v}
			}
		} else {
			err = fmt.Errorf("Data interface{} can't evaluate to string")
			return
		}

		kv.Subdirs = subdirs
		kv.Secrets = secrets
	}

	kv.getDirSecrets(c, ctx, mnt)

	return
}

// getDirSecrets() populates kvV1 subdirectory with secrets
// from kv storage version 1
// mnt: vault mount point
func (kv *kvV1) getDirSecrets(c *vault.Client, ctx context.Context, mnt string) (err error) {
	var resp *vault.Response[map[string]interface{}]
	secrets := make(map[string]*secret)

	for key := range kv.Secrets {
		path := fmt.Sprintf("%s/%s", kv.Path, key)
		uri := fmt.Sprintf("%s/%s", mnt, path)

		if resp, err = c.Read(ctx, uri); err != nil {
			err = fmt.Errorf("Get secret '%s', request failed: %s\n", uri, err)
		}

		fields := make(map[string]*string)
		for k, v := range resp.Data {
			val, _ := v.(string)
			fields[k] = &val
		}

		secrets[key] = &secret{Path: kv.Path, Name: key, Fields: fields}
	}

	kv.Secrets = secrets

	return
}

// writeDirSecretsKV2() writes all secrets from the current kvV1 directory
// into kv storage version 2
// mnt: vault mount point
func (kv *kvV1) writeDirSecretsKV2(c *vault.Client, ctx context.Context, mnt string) (err error) {
	for key, value := range kv.Secrets {
		secret := make(map[string]interface{})
		for k, v := range value.Fields {
			secret[k] = *v
		}
		_, err = c.Secrets.KvV2Write(ctx, value.Path+"/"+key, schema.KvV2WriteRequest{
			Data: secret},
			vault.WithMountPath(mnt),
		)
		if err != nil {
			return
		}
	}

	subdirs, ok := kv.Subdirs.(map[string]*kvV1)
	if !ok {
		err = fmt.Errorf("subdirs is not map[string]*kvV1")
		return
	}

	for key, value := range subdirs {
		if err = value.writeDirSecretsKV2(c, ctx, mnt); err != nil {
			err = fmt.Errorf("Failed to write directory secret '%s/%s': %s\n", value.Path, key, err)
		}
	}

	return
}
