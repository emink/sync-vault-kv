package main

import (
	"context"
	"fmt"
	"strings"

	"github.com/hashicorp/vault-client-go"
	"github.com/hashicorp/vault-client-go/schema"
)

// listDir() recursively iterate over kv storage version 2
// populating kvV2 struct while executing itself in subdirectories
// as many time as needed
// mnt: vault mount point
type kvV2 directory

func (kv *kvV2) listDir(c *vault.Client, ctx context.Context, mnt string) (err error) {
	var resp *vault.Response[map[string]interface{}]

	sURI := []string{mnt, "metadata", kv.Path}
	uri := strings.Join(sURI, "/")

	if resp, err = c.List(ctx, uri); err != nil {
		err = fmt.Errorf("List directory '%s', request failed: %s\n", uri, err)
		return
	}

	data, ok := resp.Data["keys"].([]interface{})
	if !ok {
		err = fmt.Errorf("Can't parse keys from response. Type is not []interface")
		return
	}

	subdirs := make(map[string]*kvV2)
	secrets := make(map[string]*secret)
	for k := range data {
		if v, ok := data[k].(string); ok {
			path := strings.Join([]string{kv.Path, v[:len(v)-1]}, "/")
			if v[len(v)-1:] == "/" {
				subdirs[v] = &kvV2{Path: path}
				if err = subdirs[v].listDir(c, ctx, mnt); err != nil {
					err = fmt.Errorf("Failed to list subdirectory '%s': %s\n", path, err)
					return
				}
			} else {
				secrets[v] = &secret{Path: kv.Path, Name: v}
			}
		} else {
			err = fmt.Errorf("Data interface{} can't evaluate to string")
			return
		}
		kv.Subdirs = subdirs
		kv.Secrets = secrets
	}

	kv.getDirSecrets(c, ctx, mnt)

	return
}

// getDirSecrets() populates kvV1 subdirectory with secrets
// from kv storage version 1
// mnt: vault mount point
func (kv *kvV2) getDirSecrets(c *vault.Client, ctx context.Context, mnt string) (err error) {
	var resp *vault.Response[schema.KvV2ReadResponse]
	secrets := make(map[string]*secret)

	for key := range kv.Secrets {
		uri := fmt.Sprintf("%s/%s", kv.Path, key)
		if resp, err = c.Secrets.KvV2Read(ctx, uri, vault.WithMountPath(mnt)); err != nil {
			err = fmt.Errorf("Get secret '%s', request failed: %s\n", uri, err)
		}

		fields := make(map[string]*string)
		for k, v := range resp.Data.Data {
			val, _ := v.(string)
			fields[k] = &val
		}

		secrets[key] = &secret{Path: kv.Path, Name: key, Fields: fields}
	}

	kv.Secrets = secrets

	return
}

// func (kv *kvV2) writeSecrets(c *vault.Client, ctx context.Context, mnt string) (err error) {
// 	return
// }
